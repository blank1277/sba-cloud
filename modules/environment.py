import streamlit as st
from modules.kube import *

@st.experimental_fragment
def buildPropertiesDictionary(mock = False):
    try:
        all_properties = get_all_properties(mock)
    except Exception as e:
        all_properties = None
        logging.error('Error in get_all_properties'+ str(e))
    #check if the properties are empty or none
    if all_properties is None or len(all_properties) == 0:
        return None
    list_properties = all_properties['propertySources']
    propertys_dict = {}
    for source in list_properties:
        propertys_dict[source['name']] = []
        for key, value in source['properties'].items():
            propertys_dict[source['name']].append({key: value['value']})
    # print('call buildPropertiesDictionary')
    return propertys_dict

def environment_tab(tab1, mock = False):
    propertys_dict = buildPropertiesDictionary(mock)
    if propertys_dict is None:
        st.warning('No pod properties found. Please check if the pod is running.')
    with tab1:
        with st.container(border=True):
            col_title1, col_title2 = st.columns([8, 1])
            with col_title1:
                st.write('Environment Manager')
            with col_title2:
                toggle_Multiline = st.toggle('MultiLine', False)
            if toggle_Multiline:
                properties = st.text_area('Properties', value='', height=300, key='properties', help='Enter properties in the format key=value')
            col1, col2 = st.columns([1, 1])
            with col1:
                if not toggle_Multiline:
                    property_name = st.text_input('Property Name', placeholder='Property name', label_visibility='collapsed')
                refresh_button = st.button('Refresh', key='refresh_button')
            with col2:
                if not toggle_Multiline:
                    property_value = st.text_input('Property Value', placeholder='Property value', label_visibility='collapsed',
                                            key='property_value')
                subcol0, subcol1, subcol2 = st.columns([1, 1, 1])
                with subcol1:
                    reset_button = st.button('Reset', key='reset_button',use_container_width=True)
                with subcol2:
                    update_button = st.button('Update', key='update_button',use_container_width=True)

            if update_button:
                list_name = []
                list_value = []
                if toggle_Multiline:
                    if properties is not None and len(properties) > 0:
                        properties = properties.split('\n')
                        for prop in properties:
                            if '=' in prop:
                                prop = prop.split('=')
                                list_name.append(prop[0])
                                list_value.append(prop[1])
                            else:
                                st.error('Invalid property format')
                                st.stop()
                else:
                    if property_name is None or property_name == '':
                        st.error('Property name is required')
                        st.stop()
                    if property_value is None or property_value == '':
                        st.error('Property value is required')
                        st.stop()
                    list_name.append(property_name)
                    list_value.append(property_value)
                with st.spinner('Updating properties...') as spinner:
                    for i in range(len(list_name)):
                        update_property(list_name[i], list_value[i])
                propertys_dict = buildPropertiesDictionary()


            if reset_button:
                with st.status("reseting data...") as status_text:
                    response = delete_properties()
                    if response is not None:
                        status_text.success('Properties reset successfully')
                    else:
                        status_text.error('Properties reset failed')
                    st.rerun()
            if refresh_button:
                with st.status("refreshing data...") as status_text:
                    response = refresh_properties()
                    if response is not None:
                        status_text.success('Properties refreshed successfully')
                    else:
                        status_text.error('Properties refresh failed')
                    st.rerun()
        if propertys_dict is not None:
            print_manager_properties(propertys_dict)
            for key, value in propertys_dict.items():
                print_properties_container(key,value)

def print_manager_properties(propertys_dict):
    with st.container(border=True):
        #updated properties
        manager_properties = None
        if 'manager' in propertys_dict:
            manager_properties = propertys_dict.pop('manager')
        if manager_properties is not None and len(manager_properties) > 0:
            st.info('Manager Properties', icon='✏️')
            for data in manager_properties:
                with st.container():
                    col1, col2 = st.columns([1, 1])
                    for key, value in data.items():
                        with col1:
                            st.write(key)
                        with col2:
                            st.write(value)

@st.cache_data(ttl=600)
def print_properties_container(name, properties):
    if properties is not None and len(properties) > 0:
        with st.expander(name):
            for data in properties:
                with st.container():
                    col1, col2 = st.columns([1, 1])
                    for key, value in data.items():
                        with col1:
                            st.write(key)
                        with col2:
                            st.text(value)

def update_property(property_name, property_value):
    with st.status(property_name+": updating data...") as status_text:
        responses = post_properties(property_name, property_value)
        failed = False
        st.write(responses)
        if responses is not None and len(responses) > 0:
            for response in responses:
                key, value = next(iter(response.items()))
                if value['status'] == 'error':
                    failed = True
                    break
        if not failed:
            status_text.update(label=property_name+': updated successfully', state='complete')
        else:
            status_text.update(label=property_name+': update failed', state='error')