import datetime
import streamlit as st
import os
import logging

# @st.cache_resource
def configure_logging(name,file_path, level=logging.DEBUG):
    formatter = logging.Formatter('%(asctime)s | %(name)s | %(levelname)s | %(message)s')

    logger = logging.getLogger(name)
    logging.basicConfig(level=level, format='%(asctime)s | %(name)s | %(levelname)s | %(message)s', handlers=[logging.FileHandler(file_path), logging.StreamHandler()])

    return logger

def getLogger(name=__name__, level=logging.INFO):
    datenow = datetime.datetime.now().strftime('%Y-%m-%d')
    filePath = os.getenv('LOGFILEPATH')
    if filePath is None:
        filePath = os.path.join(os.getcwd(), 'logs')
    if not os.path.exists(filePath):
        os.makedirs(filePath)
    log_file = os.path.join(filePath, f'{datenow}.log')
    logger = configure_logging(name,log_file, logging.INFO)
    return logger