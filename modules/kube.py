import streamlit as st
from kubernetes import client, config, stream
# import json
import concurrent.futures
import os, re
import subprocess
from modules.logger import logging
from base64 import b64decode


def set_kubelogin_mode(mode='interactive'):
    subprocess.run(['kubelogin', 'convert-kubeconfig', '-l', mode])
    # logging.info('Kubelogin mode set to ' + mode)

def login():
    isModeChanged = False
    try:
        # Try to load the in-cluster configuration
        config.load_incluster_config()
    except config.config_exception.ConfigException:
        # If the in-cluster configuration fails, try to load the kubeconfig file
        try:
            # Use the kubeconfig file from the current directory
            isModeChanged = True
            set_kubelogin_mode()
            config.load_kube_config()
        except config.config_exception.ConfigException:
            # If the kubeconfig file is not found, provide a fallback path
            try:
                set_kubelogin_mode('devicecode')
                isModeChanged = False
                #connect kubectl proxy
                configuration = client.Configuration()
                # Configure API key authorization: BearerToken
                configuration.api_key['authorization'] = 'Bearer ' + os.getenv('KUBE_TOKEN')
                logging.info('KUBE_TOKEN: '+os.getenv('KUBE_TOKEN'))
                configuration.proxy = "http://host.docker.internal:8600"
                configuration.client_side_validation = False
                configuration.verify_ssl = True
                with client.ApiClient(configuration) as api_client:
                    # Create an instance of the API class
                    api_instance = client.WellKnownApi(api_client)
                    try:
                        api_response = api_instance.get_service_account_issuer_open_id_configuration()
                        logging.debug('api:'+api_response)
                    except Exception as e:
                        logging.error('Exception when calling WellKnownApi->get_service_account_issuer_open_id_configuration: %s\n' % e)
                    kubeconfig_path = os.path.join(os.path.expanduser('/app'), '.kube', 'config')
                    st.session_state.client_config = configuration
                    client.Configuration.set_default(configuration)
                    logging.debug(st.session_state.client_config)
                    # config.load_kube_config(config_file=kubeconfig_path, client_configuration=configuration, persist_config=False)
            except config.config_exception.ConfigException:
                st.error('Kubernetes configuration not found. Please check if the Kubernetes configuration is available.')
                st.stop()
        finally:
            if isModeChanged:
                set_kubelogin_mode("devicecode")
            # print('Kubeconfig file loaded')
    st.session_state.is_logged_in = True
    st.rerun()

def get_list_namespaced_pod(env):
    v1 = getClientCoreV1Api()
    try:
        list_namespaced_pod = v1.list_namespaced_pod(namespace='scb-fasteasy-cloud-'+env, watch=False)
    except Exception as e:
        logging.error('Error in get_list_namespaced_pod'+ str(e))
        list_namespaced_pod = None
    return list_namespaced_pod

def getClientCoreV1Api():
    if 'client_config' in st.session_state:
        v1 = client.CoreV1Api(client.ApiClient(st.session_state.client_config, header_name='authorization', header_value='Bearer ' + os.getenv('KUBE_TOKEN')))
    else:
        v1 = client.CoreV1Api()
    return v1

def eval_response(response):
    try:
        json_str = eval(response)
        return json_str
    except Exception as e:
        print('Error in response,', e)
        return None
    
def get_all_properties(mock=False):
    if mock:
        with open('response.json') as f:
            properties = f.read()
        return eval(properties)
    v1 = getClientCoreV1Api()
    pod_name = st.session_state.pod_id
    env = st.session_state.env
    container = st.session_state.container_name
    exec_command = [
        '/bin/bash',
        '-c',
        'curl -s --location --request GET "http://localhost:9090/env" --header "Content-Type: application/json"'
    ]
    resp = stream.stream(v1.connect_get_namespaced_pod_exec, pod_name, 'scb-fasteasy-cloud-'+env, command=exec_command, container=container, stderr=True, stdin=False, stdout=True, tty=False)
    return eval_response(resp)
    

def post_properties(property_name, property_value):
    pod_name = st.session_state.pod_id
    env = st.session_state.env
    container = st.session_state.container_name
    update_all_pods = st.session_state.update_all
    resps = []
    pods = []
    # get all pod at the same container_name
    if update_all_pods:
        list_namespaced_pod = get_list_namespaced_pod(env)
        if list_namespaced_pod is None:
            logging.error('Error in get_list_namespaced_pod is None: update only one pod')
            pods.append(pod_name)
        for i in list_namespaced_pod.items:
            if i.metadata.labels['app'] == container:
                pods.append(i.metadata.name)
        with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
            futures = [executor.submit(_post_properties, property_name, property_value, env, pod, container) for pod in pods]
            for future in concurrent.futures.as_completed(futures):
                try:
                    pod_name, data = future.result()
                    resps.append({pod_name:data})
                except Exception as exc:
                    resps.append({pod_name:exc})
    else:
        pod_name, resp = _post_properties(property_name, property_value, env, pod_name, container)
        resps.append({pod_name:resp})
    return resps

def _post_properties(property_name, property_value, env, pod_name, container):
    v1 = getClientCoreV1Api()
    exec_command = [
                    '/bin/bash',
                    '-c',
                    'curl -s -v --location --request POST "http://localhost:9090/env" --header "Content-Type: application/json" --data-raw "{\\"name\\": \\"'+property_name+'\\", \\"value\\": \\"'+property_value+'\\"}"'
                ]
    try:
        resp = stream.stream(v1.connect_get_namespaced_pod_exec, pod_name, 'scb-fasteasy-cloud-'+env, command=exec_command, container=container, stderr=True, stdin=False, stdout=True, tty=False)
        resp = set_response_status(resp)
    except Exception as e:
        resp = {
            'status': 'error',
            'body': str(e)
        }
    
    return pod_name, resp


def set_response_status(response):
    status = 'success'
    # Extract HTTP status for any version
    status_match = re.search(r'< HTTP/\d\.\d (\d+)', response)
    http_status = status_match.group(1) if status_match else None
    if http_status is None or http_status != '200':
        status = 'error'
    # Extract body (everything after the last HTTP response header line)
    body_match = re.search(r'{\s*".*?"\s*}', response)
    body = body_match.group().strip() if body_match else None
    resp = {
        'status': status,
        'body': body
    }
    return resp

def delete_properties():
    pod_name = st.session_state.pod_id
    env = st.session_state.env
    container = st.session_state.container_name
    update_all_pods = st.session_state.update_all
    resps = []
    pods = []
    # get all pod at the same container_name
    if update_all_pods:
        list_namespaced_pod = get_list_namespaced_pod(env)
        if list_namespaced_pod is None:
            logging.error('Error in get_list_namespaced_pod is None: update only one pod')
            pods.append(pod_name)
        for i in list_namespaced_pod.items:
            if i.metadata.labels['app'] == container:
                pods.append(i.metadata.name)
        with concurrent.futures.ThreadPoolExecutor(max_workers=len(pods)) as executor:
            futures = [executor.submit(_delete_properties, env, pod, container) for pod in pods]
            for future in concurrent.futures.as_completed(futures):
                try:
                    pod_name, data = future.result()
                    resps.append({pod_name:data})
                except Exception as exc:
                    resps.append({pod_name:exc})
    else:
        pod_name, resp = _delete_properties(env, pod_name, container)
        resps.append({pod_name:resp})
    return resps

def _delete_properties(env, pod_name, container):
    v1 = getClientCoreV1Api()
    exec_command = [
        '/bin/bash',
        '-c',
        'curl -s --location --request DELETE "http://localhost:9090/env" --header "Content-Type: application/json"'
    ]
    resp = stream.stream(v1.connect_get_namespaced_pod_exec, pod_name, 'scb-fasteasy-cloud-'+env, command=exec_command, container=container, stderr=True, stdin=False, stdout=True, tty=False)
    return pod_name,resp

def refresh_properties():
    pod_name = st.session_state.pod_id
    env = st.session_state.env
    container = st.session_state.container_name
    update_all_pods = st.session_state.update_all
    resps = []
    pods = []
    # get all pod at the same container_name
    if update_all_pods:
        list_namespaced_pod = get_list_namespaced_pod(env)
        if list_namespaced_pod is None:
            logging.error('Error in get_list_namespaced_pod is None: update only one pod')
            pods.append(pod_name)
        for i in list_namespaced_pod.items:
            if i.metadata.labels['app'] == container:
                pods.append(i.metadata.name)
        with concurrent.futures.ThreadPoolExecutor(max_workers=len(pods)) as executor:
            futures = [executor.submit(_refresh_properties, env, pod, container) for pod in pods]
            for future in concurrent.futures.as_completed(futures):
                try:
                    pod_name, data = future.result()
                    resps.append({pod_name:data})
                except Exception as exc:
                    resps.append({pod_name:exc})
    else:
        pod_name, resp = _refresh_properties(env, pod_name, container)
        resps.append({pod_name:resp})
    return resps

def _refresh_properties(env, pod_name, container):
    v1 = getClientCoreV1Api()
    exec_command = [
        '/bin/bash',
        '-c',
        'curl -s --location --request POST "http://localhost:9090/refresh" --header "Content-Type: application/json"'
    ]
    resp = stream.stream(v1.connect_get_namespaced_pod_exec, pod_name, 'scb-fasteasy-cloud-'+env, command=exec_command, container=container, stderr=True, stdin=False, stdout=True, tty=False)
    return pod_name, resp