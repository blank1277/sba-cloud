import streamlit as st

def setting_tab(tab2):
    if 'update_all' not in st.session_state:
        st.session_state.update_all = True
    with tab2:
        st.write('Settings')
        col1, col2 = st.columns([1, 1])
        with col1:
            st.write('Update for all pods')
        with col2:
            st.toggle('Update All', st.session_state.update_all, on_change=update_all_toggle_changed, help='This feature in development may not work as expected')
        
        
            

def update_all_toggle_changed():
    if st.session_state.update_all:
        st.session_state.update_all = False
    else:
        st.session_state.update_all = True
