import streamlit as st
from streamlit_extras.add_vertical_space import add_vertical_space
from streamlit_extras.st_keyup import st_keyup
from modules.kube import *
from modules.logger import getLogger

LIST_OF_ENV = ['dev01', 'alpha', 'staging']
DEFAULT_ENV = 1

# Top bar
def block_microservice(name, pods=[]):
    with st.expander(name):
        pod_container = st.container()
        with pod_container:
            col1, col2, col3 = st.columns([4, 1, 2])
            for pod in pods:
                with col1:
                    if st.button(pod['pod'], key=pod['pod'], use_container_width=True, on_click=pod_selected, args=[name, pod['pod'], pod['app_version']], disabled=pod['status'] != 'Running'):
                        st.switch_page('pages/pod-details.py')
                with col2:
                    st.button(pod['ready'], key=pod['pod']+'ready', use_container_width=True, disabled=True)
                with col3:
                    if pod['status'] == 'Running':
                        st.button(':green['+pod['status']+']', key=pod['pod']+'status', use_container_width=True, disabled=True)
                    else:
                        st.button(':orange['+pod['status']+']', key=pod['pod']+'status', use_container_width=True, disabled=True)

def pod_selected(name, pod_id, app_version):
    st.session_state.pod_id = pod_id
    st.session_state.container_name = name
    st.session_state.app_version = app_version
    

def get_list_kube_pods(env,mock=False):
    if mock:
        with open('microservices_pods.txt') as f:
            microservices_pods = f.read()
        return eval(microservices_pods)
    microservices_pods = {}
    list_namespaced_pod = get_list_namespaced_pod(env)
    if list_namespaced_pod is None:
        streamlit_logger.error('Error list_namespaced_pod is None')
        return microservices_pods
    for i in list_namespaced_pod.items:
        microservice_name = i.metadata.labels['app']
        pod_name = i.metadata.name
        app_version = i.metadata.labels['appVersion']
        pod_status = i.status.phase
        container_statuses = i.status.container_statuses
        # container_name = i.spec.containers[-1].name
        ready = 0 
        for j in container_statuses:
            if j.ready:
                ready += 1
            else:
                if j.state.waiting != None:
                    pod_status = j.state.waiting.reason
                elif j.state.terminated != None:
                    pod_status = j.state.terminated.reason
                else:
                    pod_status = "Error"
        ready =  str(ready)+ "/" + str(len(i.spec.containers))
        if microservice_name not in microservices_pods:
            microservices_pods[microservice_name] = [
                {   'pod': pod_name,
                    'ready': ready,
                    'status': pod_status,
                    'app_version': app_version,
                    # 'container_name': container_name
                }]
        else:
            microservices_pods[microservice_name].append(
                {   'pod': pod_name,
                    'ready': ready,
                    'status': pod_status,
                    'app_version': app_version,
                    # 'container_name': container_name
                })
    return microservices_pods
        
def display_microservices(microservices):
    add_vertical_space(1)
    for microservice in microservices:
        service_container = st.container()
        status = True
        status_index = 0
        for i in range(len(microservices[microservice])):
            if microservices[microservice][i]['status'] != 'Running' \
                or microservices[microservice][i]['ready'].split('/')[0] != microservices[microservice][i]['ready'].split('/')[1]:
                logging.error('Microservice '+microservice+' is not running')
                status = False
                status_index = i
                break
        with service_container:
            col1, col2 = st.columns([8, 3])
            with col1:
                block_microservice(microservice, microservices[microservice])
                
            with col2:
                if status:
                    st.success(microservices[microservice][status_index]['status'], icon='✅')
                else:
                    st.warning(microservices[microservice][status_index]['status'], icon='⚠️')
        

def app():
    # Set page title and favicon
    st.set_page_config(
        page_title='Spring Boot Admin Dashboard',
        page_icon=':bar_chart:',
        layout='wide',
        # initial_sidebar_state="collapsed"
    )
    st.sidebar.page_link('dashboard.py', label='Dashboard')
    st.sidebar.divider()
    # streamlit_logger.info('Spring Boot Admin Dashboard Started.') 
    if 'is_logged_in' not in st.session_state or not st.session_state.is_logged_in:
        # st.warning('Please login to access the dashboard.')
        # if st.button('Login'):
        #     login()
        # st.stop()
        st.spinner('Please wait, logging in...')
        st.session_state.env = LIST_OF_ENV[DEFAULT_ENV]
        login()
    top_bar = st.container()
    with top_bar:
        col1, col2, col3 = st.columns([1, 5, 1])
        with col1:
            st.image('logo.png', width=100)  # Replace with your logo image
        with col2:
            st.title('Spring Boot Admin Dashboard')
        with col3:
            env_box = st.selectbox('Environment', LIST_OF_ENV, DEFAULT_ENV)
            if env_box:
                st.session_state.env = env_box     
    microservices = get_list_kube_pods(env_box,False)
    if microservices == {}:
        st.warning('No microservices found.')
        st.stop()
    #search microservices
    search = st_keyup('Search Microservices')
    if search:
        microservices_searched = {k: v for k, v in microservices.items() if search in k}
        display_microservices(microservices_searched)
    else:
        display_microservices(microservices)

if __name__ == '__main__':
    streamlit_logger = getLogger(__name__)
    app()

