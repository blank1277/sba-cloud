1. ```pip install -r requirements.txt```
2. ```streamlit run dashboard.py```

- Note: need kubeconfig file (./kube/config) and connect AWS VPN.

for docker 
1. create proxy
   ```kubectl proxy --port=8600 --address='192.168.1.167' --accept-hosts='^localhost$,^127\.0\.0\.1$,^\[::1\]$,^192\.168\.1\.167$,^host\.docker\.internal$'```
2. change ip on docker-compose