import streamlit as st
from streamlit_extras.st_keyup import st_keyup
from modules.setting import *
from modules.environment import *
from streamlit_extras.add_vertical_space import add_vertical_space

def app(mock=False):
    if mock:
        st.session_state.pod_id = 'addresses-589fbffd54-j84sw'
        st.session_state.container_name = 'addresses'
        st.session_state.env = 'dev01'
        st.session_state.is_logged_in = True
    
    if 'is_logged_in' not in st.session_state or not st.session_state.is_logged_in:
        st.warning('Please login to access the dashboard.')
        st.stop()

    if 'pod_id' not in st.session_state:
        st.warning('Please select a pod to view its details.')
        st.stop()
    if 'update_all' not in st.session_state:
        st.session_state.update_all = False
    tab1, tab2 = st.tabs(["Environments", "Settings"])
    st.sidebar.page_link('dashboard.py', label='Dashboard', use_container_width=True)
    st.sidebar.divider()
    with st.sidebar.expander('$ \\text{'+st.session_state.container_name+'}$', expanded=True):
    # with st.sidebar.expander('test ***'+str(st.session_state.container_name)+'***', expanded=True):
        st.write('**Pod**: '+st.session_state.pod_id)
        st.write('**Environment**: '+st.session_state.env)
        st.write('**App Version**: '+str(st.session_state.app_version))
        st.write('**Update All**: '+str(st.session_state.update_all))
    environment_tab(tab1,mock)
    setting_tab(tab2)

if __name__ == '__main__':
    app(False)